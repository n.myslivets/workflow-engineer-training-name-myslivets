# NIkita Myslivets
### I am begginer programmer. I enjoy learning and putting new knowledge into practice as quickly as I can. By the way I am a professional athlete.
#### __My skills:__

skill  | mark
:------|-----:
Python | 4
mySQL  | 3
yaml   | 1
csv    | 1 
md     | 4
git    | 4
redmine| 2
jira   | 1

#### I am working on my tasks from Monday to Friday from 14 to 22 hours because from 9 to 13 I have a study. 
#### My links:
- [Githab](https://github.com/NikitaMyslivets)
- [GitLab](https://gitlab.com/n.myslivets)
- [Jira](https://workflow-engineers-syberry.atlassian.net/jira/people/6034cb63d416ea0070f0a057)
- [Readmine](https://academy-redmine.jarvis.syberry.net/my/page)
- Discord: NikitaMyslivets#2992